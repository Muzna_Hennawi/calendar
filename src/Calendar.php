<?php
namespace muzna\gccalendar;

//use CalendarAsset;
use yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

class Calendar extends Widget
{
/**
 *
 */
  public $dayNames =['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
  /**
   *
  */
    public $monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    /**
     *
     */
    public $nextIcon = '&gt;';

    /**
     *
     */
    public $prevIcon = '&lt;';

    /**
     *is the Integer that you can use for the beginning of date.
     * 0 is Sunday
     * 6 is Saturday
     */
public  $dateBegin = 0;
/*
 * on prev momth
 */
    public $onPrevMonth= '';
    public $onNextMonth='';
    public $onclickDate= '';
    public $selector;
    public $clientOptions=[];

public $events =[];
    public function run()
    {
                echo '<div id="cla"></div>';
        $this->registerClientScript();
    }

    protected function registerClientScript()
    {
        $js = [];
        $view = $this->getView();
        //$id= $this->selector;

        CalendarAsset::register($view);

        if ($this->dayNames !== null ){
            $this->clientOptions['dayNames']= $this->dayNames;
        }
        if ($this->dateBegin != null){
            $this->clientOptions['dateBegin']= $this->dateBegin;
        }
        if($this->monthNames != null){
            $this->clientOptions['monthNames']= $this->monthNames;
        }
        if($this->nextIcon !=null){
            $this->clientOptions['nextIcon']=$this->nextIcon;
        }
        if($this->prevIcon !=null){
            $this->clientOptions['prevIcon']=$this->prevIcon;
        }
        if($this->events !=null){
           $this->clientOptions['events']= $this->events;
        }
        if($this->onPrevMonth !=null){
            $this->clientOptions['onPrevMonth']= $this->onPrevMonth;
        }
        if($this->onNextMonth !=null){
            $this->clientOptions['onNextMonth']= $this->onNextMonth;
        }
        if($this->onclickDate !=null){
            $this->clientOptions['onclickDate']= $this->onclickDate;
        }

            $options = Json::encode($this->clientOptions);


        $js[]= '$("#cal").calendarGC('.$options.');';

        $view->registerJs(implode("\n", $js));
    }
}