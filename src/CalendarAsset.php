<?php
namespace muzna\gccalendar;

use yii\web\AssetBundle;

class CalendarAsset extends AssetBundle
{
 public $sourcePath = '@vendor/muzna/yii2-gc-calendar/src/assets';
   
    public $css = [
       'css/calendar-gc.min.css',
    ];
    public $js = [
        'js/jquery-3.6.0.min.js',
        'js/calendar-gc.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}

