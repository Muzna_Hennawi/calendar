# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

this is animated Calendar library depends on GcCalendar javascript library

### How do I get set up? ###

* run
```
composer require muzna/yii2-gc-calendar
```
* or add to Composer
```
 "muzna/yii2-gc-calendar": "dev-master",
```

### usage ###
there are to ways ro use the extension:
* register your view in the CalendarAsset
```
\muzna\gccalendar\CalendarAsset::register($this);
```
and then use the full GcCalendar attributes
```
$("yourElement").calendarGC(options);
```

* use Calendar Widget :
 ```
 \muzna\gccalendar\Calendar::widget([
    'selector'=>'cal',
    'dateBegin'=> 2,
    'prevIcon'=> '&#x3c;',
    'nextIcon'=> '&#x3e;',
    'events'=>$events,


])
 ```


